#define INLINE	inline

#define YES	(1)
#define NO	(0)
typedef unsigned int BOOLEAN;
typedef unsigned long long ULONGLONG;

/*
 * Board
 */
#define board_offset(x, y)	((board_width+1)*(y)+(x))
#define board_pos(x, y)		(board + board_offset(x, y))
#define WALL	(INT_MIN)
#define EMPTY	(0)

int board_width;
int board_height;
int board_size0;
int* board0;
int board_size;
int* board;
int board_empty_count;

/*
 * Tile
 */
#define TILES_MAX	(100)
#define TILE_TYPES_MAX	(100)
typedef struct tile {
    char name;
    int* shape;
    int id;
    int type;
    int* pos;
    int dirs;
} TILE;
int tiles_size = 0;
TILE* tiles[TILES_MAX];
int tile_type_count = 1;
int tile_types[TILE_TYPES_MAX];

/*
 * Encoder and Hash table
 */
typedef struct encode_struct {
    ULONGLONG code;
    struct encode_struct *next;
} ENCODE_TABLE;
ENCODE_TABLE* encode_table_entry;
int encode_table_size;
ENCODE_TABLE** encode_table;

#define HASH_UNDEF_KEY	((HASH_KEY)(-1))
#define hash_func(key)	((key) % hash_entry_size)
typedef ULONGLONG HASH_KEY;
typedef short HASH_VALUE;
typedef struct hash_data {
    HASH_KEY key : 48;
    HASH_VALUE val : 16;
    struct hash_data *next;
} HASH_DATA;
HASH_DATA** hash_entry;
int hash_entry_size;
HASH_DATA* hash_data;
int hash_data_size;
int hash_data_count;
int hash_count;
HASH_KEY hash_last_key = HASH_UNDEF_KEY;
HASH_DATA* hash_last_data;

/*
 * Solver
 */
#define MAX_GOALS	(100)

int bsearch_depth;
int search_depth;
int sol_depth = INT_MAX;
int sol_count = 0;
int time0;
double cpu0;
int goal_size;
TILE* goal_tile[MAX_GOALS];
int* goal_pos[MAX_GOALS];
int dx[] = {0, 0, -1, 1};
int dy[] = {-1, 1, 0, 0};
int dn[] = {'U', 'D', 'L', 'R'};
int moves_size;
typedef struct {
    TILE* tile;
    int dir;
} MOVE;
MOVE *moves;

/*
 * Prototype declarations
 */
double usertime(void);
void error(char*);

void board_make(int, int);
void board_set(int, int*);
TILE* board_add_tile(TILE*);
void board_set_tile(TILE*, int*);
void board_init(void);
void board_remove_tile(TILE*);
BOOLEAN board_move_tile(TILE*, int*);
HASH_KEY board_encode(void);
void board_print(void);

TILE* tile_make(char, char*, int);

void encode_init(int[], int);
ENCODE_TABLE* encode_table_make(int[], int[], int);
ULONGLONG rp(int[], int);
ULONGLONG factorial(int);

void hash_init(int, int);
HASH_VALUE* hash_get(HASH_KEY, HASH_VALUE*);
void hash_put(HASH_KEY, HASH_VALUE);
void hash_remove(HASH_KEY);
void hash_reset(void);

void moves_print(void);
INLINE BOOLEAN goal(int);
void search(int, TILE*, int);
void move_tile(TILE*, int, int, int, int);
void solve(int, int);
TILE* add_tile(int, int, char, char*, int);
