/*
 * Sliding Block Puzzle Solver
 * by Naoyuki Tamura
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <limits.h>
#include <time.h>

#include "sliding.h"

/****************************************************************
 * Utilities
 ****************************************************************/
double usertime() {
    return (clock() / CLOCKS_PER_SEC);
}

void error(char* msg) {
    printf("%s\n", msg);
    exit(1);
}

/****************************************************************
 * Board
 ****************************************************************/
void board_make(int w, int h) {
    int* pos;
    int x, y, i;

    board_width = w;
    board_height = h;
    board_size0 = (board_width+1)*(board_height+2);
    board0 = malloc(sizeof(int)*board_size0);
    if (board0 == NULL)
        error("Cannot allocate board");
    board = board0 + board_width + 1;
    board_size = (board_width+1)*board_height;
    for (pos = board0; pos < board0+board_size0; ++pos)
        *pos = WALL;
    for (y = 0; y < board_height; ++y)
        for (x = 0; x < board_width; ++x)
            *board_pos(x, y) = EMPTY;
    for (i = 0; i < TILE_TYPES_MAX; ++i)
        tile_types[i] = 0;
}

void board_set(int val, int* pos) {
    *pos = val;
}

TILE* board_add_tile(TILE* tile) {
    int i, j;
    int* shape;
    int* shape2;

    if (tiles_size >= TILES_MAX)
        error("Too many tiles");
    tile->id = tiles_size + 1;
    tile->type = 0;
    for (j = 0; j < tiles_size && tile->type == 0; ++j) {
        shape = tile->shape;
        shape2 = tiles[j]->shape;
        for (; *shape >= 0 && *shape2 >= 0; ++shape, ++shape2)
            if (*shape != *shape2)
                break;
        if (*shape < 0 && *shape2 < 0) {
            tile->type = tiles[j]->type;
            break;
        }
    }
    if (tile->type == 0)
        tile->type = tile_type_count++;
    tiles[tiles_size++] = tile;
    ++tile_types[tile->type];
    return tile;
}

TILE* board_find_tile(char c) {
    for (int j = 0; j < tiles_size; j++) {
        if (tiles[j]->name == c)
            return tiles[j];
    }
    return NULL;
}

void board_set_tile(TILE* tile, int* pos) {
    int* shape;
    int ti = tile->id;

    shape = tile->shape;
    pos[*shape++] = ti;
    for (; *shape >= 0; ++shape)
        pos[*shape] = -ti;
    tile->pos = pos;
    return;
}

void board_init() {
    int* pos;

    board_empty_count = 0;
    for (pos = board; pos < board+board_size; ++pos)
        if (*pos == EMPTY)
            ++board_empty_count;
    tile_types[0] = board_empty_count;
    encode_init(tile_types, tile_type_count);
}

INLINE void board_remove_tile(TILE* tile) {
    int* pos = tile->pos;
    int* shape;

    for (shape = tile->shape; *shape >= 0; ++shape)
        pos[*shape] = EMPTY;
}

INLINE BOOLEAN board_move_tile(TILE* tile, int* pos1) {
    int* pos0 = tile->pos;
    int* shape;
    int ti = tile->id;

    for (shape = tile->shape; *shape >= 0; ++shape)
        if (pos1[*shape] != EMPTY && abs(pos1[*shape]) != ti)
            return NO;
    for (shape = tile->shape; *shape >= 0; ++shape)
        pos0[*shape] = EMPTY;
    shape = tile->shape;
    pos1[*shape++] = ti;
    for (; *shape >= 0; ++shape)
        pos1[*shape] = -ti;
    tile->pos = pos1;
    return YES;
}

INLINE HASH_KEY board_encode() {
    int* pos;
    ENCODE_TABLE* p;
    int t;
    HASH_KEY code = 0;

    p = encode_table_entry;
    for (pos = board; pos < board+board_size; ++pos) {
        if (*pos == WALL || *pos < 0)
            continue;
        if (*pos == EMPTY)
            t = 0;
        else 
            t = tiles[*pos-1]->type;
        code += p[t].code;
        p = p[t].next;
    }
    return code;
}

void board_print() {
    int cell, x, y;
    char c;

    for (y = 0; y < board_height; ++y) {
        for (x = 0; x < board_width; ++x) {
            cell = *board_pos(x, y);
            if (cell == WALL)
                c = ' ';
            else if (cell == EMPTY)
                c = '.';
            else
                c = tiles[abs(cell)-1]->name;
            printf("%c ", c);
        }
        printf("\n");
    }
}

/****************************************************************
 * Tile
 ****************************************************************/
TILE* tile_make(char c, char* s, int dirs) {
    TILE* tile;
    int* shape;
    int x0, y0, x, y;

    if (tiles_size >= TILES_MAX)
        error("Too many tiles");
    tile = malloc(sizeof(TILE));
    if (tile == NULL)
        error("Cannot allocate tile");
    tile->name = c;
    tile->dirs = dirs;
    shape = malloc(sizeof(int)*(strlen(s)+1));
    if (shape == NULL)
        error("Cannot allocate tile");
    tile->shape = shape;
    tile->id = 0;
    tile->type = 0;
    tile->pos = NULL;
    x = y = 0;
    for (; *s != '\0'; ++s) {
        if (*s == '\n') {
            x = 0; ++y;
        } else if (*s == '.' || *s == ' ') {
            ++x;
        } else
            break;
    }
    x0 = x; y0 = y;
    for (; *s != '\0'; ++s) {
        if (*s == '\n') {
            x = 0; ++y;
        } else if (*s == '.' || *s == ' ') {
            ++x;
        } else {
            *shape++ = board_offset(x-x0, y-y0);
            ++x;
        }
    }
    *shape++ = -1;
    return tile;
}

/****************************************************************
 * Encode
 ****************************************************************/
void encode_init(int c[], int n) {
    int i;
    int* c0;

    c0 = malloc(sizeof(int)*n);
    if (c0 == NULL)
        error("Cannot allocate");
    encode_table_size = 1;
    for (i = 0; i < n; ++i) {
        c0[i] = c[i];
        encode_table_size *= c0[i] + 1;
    }
    encode_table = malloc(sizeof(ENCODE_TABLE*)*encode_table_size);
    if (encode_table == NULL)
        error("Cannot allocate encode_table");
    for (i = 0; i < encode_table_size; ++i)
        encode_table[i] = NULL;
    encode_table_entry = encode_table_make(c, c0, n);
}

ENCODE_TABLE* encode_table_make(int c[], int c0[], int n) {
    int k, i, empty;
    ENCODE_TABLE* p;
    ULONGLONG code;

    k = 0;
    empty = 1;
    for (i = 0; i < n; ++i) {
        k = k*(c0[i]+1) + c[i];
        empty &= (c[i] == 0);
    }
    if (empty)
        return NULL;
    if (encode_table[k] != NULL)
        return encode_table[k];
    p = encode_table[k] = malloc(sizeof(ENCODE_TABLE)*n);
    if (p == NULL)
        error("Cannot allocate encode_table");
    code = 0;
    for (i = 0; i < n; ++i) {
        p[i].code = code;
        p[i].next = NULL;
        if (c[i] > 0) {
            --c[i];
            p[i].next = encode_table_make(c, c0, n);
            code += rp(c, n);
            ++c[i];
        }
    }
    return p;
}

ULONGLONG rp(int c[], int n) {
    int i, sum;
    ULONGLONG r;

    sum = 0;
    for (i = 0; i < n; ++i)
        sum += c[i];
    r = 1;
    for (i = 0; i < n; ++i)
        r *= factorial(c[i]);
    r = factorial(sum) / r;
    return r;
}

ULONGLONG factorial(int n) {
    int i;
    ULONGLONG f = 1;

    for (i = 2; i <= n; ++i)
        f *= i;
    return f;
}

/****************************************************************
 * Hash table
 ****************************************************************/
void hash_init(int entries, int size) {
    int k;
    int fd;

    hash_entry_size = entries;
    hash_data_size = size;
    printf("Hash table %d entries, %d elements, %ld bytes\n",
           hash_entry_size, hash_data_size, sizeof(HASH_DATA)*hash_data_size);
    hash_entry = malloc(sizeof(HASH_DATA*)*hash_entry_size);
    if (hash_entry == NULL)
        error("Cannot allocate hash entry table");
    for (k = 0; k < hash_entry_size; ++k)
        hash_entry[k] = NULL;
    hash_data = malloc(sizeof(HASH_DATA)*hash_data_size);
    if (hash_data == NULL)
        error("Cannot allocate hash data table");
}

INLINE HASH_VALUE* hash_get(HASH_KEY key, HASH_VALUE* p) {
    HASH_DATA* hp;

    if (key == hash_last_key) {
        hp = hash_last_data;
        if (hp == NULL)
            return NULL;
        *p = hp->val;
        return p;
    }
    hash_last_key = key;
    for (hp = hash_entry[hash_func(key)]; hp != NULL; hp = hp->next) {
        if (key == hp->key) {
            hash_last_data = hp;
            *p = hp->val;
            return p;
        }
    }
    hash_last_data = NULL;
    return NULL;
}

INLINE void hash_put(HASH_KEY key, HASH_VALUE val) {
    int k;
    HASH_VALUE dummy;
    HASH_DATA* hp;

    if (hash_last_key != key)
        hash_get(key, &dummy);
    hp = hash_last_data;
    if (hp != NULL) {
        hp->val = val;
        return;
    }
    if (hash_data_count >= hash_data_size)
        error("Hash table overflow");
    k = hash_func(key);
    hp = hash_data + hash_data_count;
    ++hash_data_count;
    hp->key = key;
    hp->val = val;
    hp->next = hash_entry[k];
    hash_entry[k] = hp;
    hash_last_data = hp;
}

INLINE void hash_remove(HASH_KEY key) {
    HASH_DATA** p;

    p = &hash_entry[hash_func(key)];
    while (*p != NULL) {
        if (key == (*p)->key) {
            *p = (*p)->next;
            return;
        }
        p = &((*p)->next);
    }
}

void hash_reset() {
    int k;
    HASH_DATA* hp;

    for (k = 0; k < hash_entry_size; ++k) {
        for (hp = hash_entry[k]; hp != NULL; hp = hp->next) {
            hp->val = abs(hp->val);
        }
    }
}

/****************************************************************
 * Solver
 ****************************************************************/
void moves_print() {
    int i;
    TILE* tile = NULL;

    for (i = 0; i < moves_size; ++i) {
        if (moves[i].tile != tile) {
            if (tile != NULL)
                putchar(' ');
            putchar(moves[i].tile->name);
        }
        tile = moves[i].tile;
        putchar(dn[moves[i].dir]);
    }
    putchar('\n');
}


INLINE BOOLEAN goal(int via) {
    return goal_tile[via]->pos == goal_pos[via];
}

void search(int depth, TILE* tile0, int via) {
    HASH_KEY code;
    HASH_VALUE val;
    int i;

    if (depth > search_depth || depth >= sol_depth)
        return;
    if (goal(via)) {
        ++via;
        if (via == goal_size) {
            ++sol_count;
            sol_depth = depth;
            printf("***** Solution %d (depth=%d)\n", sol_count, sol_depth);
            moves_print();
            board_print();
            printf("\n");
            fflush(stdout);
            return;
        }
    }
    code = board_encode() * goal_size + via;
    if (hash_get(code, &val) == NULL) {
        /* not visited */
    } else if (val >= 0) {
        /* not visited in this search */
        if (val < depth)
            return;
    } else {
        /* visited in this search */
        if (-val <= depth)
            return;
    }
    /* mark visited */
    hash_put(code, -depth);
    if (search_depth > bsearch_depth && depth == 40) {
        moves_print();
    }
    for (i = 0; i < tiles_size; ++i) {
        if (tile0 == tiles[i])
            continue;
        move_tile(tiles[i], -1, board_empty_count, depth, via);
    }
    if (depth > bsearch_depth) {
        hash_remove(code);
    }
}

void move_tile(TILE* tile, int dir0, int c, int depth, int via) {
    int dir;
    int* old_pos;

    if (c <= 0)
        return;
    old_pos = tile->pos;
    for (dir = 0; dir < 4; ++dir) {
        if (dir0 == (dir ^ 1))
            continue;
        if ((tile->dirs & (1 << dir)) == 0)
            continue;
        if (board_move_tile(tile, old_pos+board_offset(dx[dir], dy[dir]))) {
            moves[moves_size].tile = tile;
            moves[moves_size].dir = dir;
            ++moves_size;
            search(depth+1, tile, via);
            --moves_size;
            board_move_tile(tile, old_pos);
        }
    }
    for (dir = 0; dir < 4; ++dir) {
        if (dir0 == (dir ^ 1))
            continue;
        if ((tile->dirs & (1 << dir)) == 0)
            continue;
        if (board_move_tile(tile, old_pos+board_offset(dx[dir], dy[dir]))) {
            moves[moves_size].tile = tile;
            moves[moves_size].dir = dir;
            ++moves_size;
            move_tile(tile, dir, c-1, depth, via);
            --moves_size;
            board_move_tile(tile, old_pos);
        }
    }
}

void solve(int max_depth, int inc_depth) {
    int depth;
    double cpu, usage;

    moves = malloc(sizeof(MOVE)*max_depth*100);
    if (moves == NULL)
        error("Cannot allocate moves");
    time0 = time(NULL);
    cpu0 = usertime();
    sol_count = 0;
    for (depth = inc_depth; depth <= max_depth; depth += inc_depth) {
        if (sol_count > 0)
            break;
        hash_reset();
        moves_size = 0;
        bsearch_depth = depth;
        search_depth = depth;
        search(0, NULL, 0);
        printf("bsearch_depth=%d ", bsearch_depth);
        printf("search_depth=%d ", search_depth);
        printf("time=%ld ", time(NULL)-time0);
        /* printf("CPU=%d ", (int)(usertime()-cpu0)); */
        usage = (100.0*hash_data_count)/hash_data_size;
        printf("states=%d (%.1f%%)", hash_data_count, usage);
        printf("\n");
    }
    if (sol_count == 0) {
        hash_reset();
        moves_size = 0;
        bsearch_depth = max_depth;
        search_depth = 60;
        search(0, NULL, 0);
    }
}

TILE* add_tile(int x, int y, char name, char* s, int dirs) {
    TILE* tile;

    tile = tile_make(name, s, dirs);
    board_add_tile(tile);
    board_set_tile(tile, board_pos(x, y));
    return tile;
}

char* scan_word(char* p, char* w) {
    while (isspace(*p))
        p++;
    if (*p == '\n' || *p == '\0')
        return NULL;
    while (! isspace(*p) && *p != '\0')
        *w++ = *p++;
    *w = '\0';
    return p;
}

int read_puzzle(void) {
    char buf[BUFSIZ];
    char wd[BUFSIZ];
    char* p;

    goal_size = 1;
    while (1) {
        if (fgets(buf, BUFSIZ, stdin) == NULL)
            break;
        p = buf;
        p = scan_word(p, wd);
        if (p == NULL || wd[0] == '#')
            continue;
        if (strcmp(wd, "BOARD") == 0) {
            int w, h;
            p = scan_word(p, wd);
            w = atoi(wd);
            p = scan_word(p, wd);
            h = atoi(wd);
            board_make(w, h);
        } else if (strcmp(wd, "WALL") == 0) {
            int x, y;
            p = scan_word(p, wd);
            x = atoi(wd);
            p = scan_word(p, wd);
            y = atoi(wd);
            board_set(WALL, board_pos(x, y));
        } else if (strcmp(wd, "TILE") == 0) {
            int x, y;
            char name;
            char s[BUFSIZ];
            int dirs;
            p = scan_word(p, wd);
            x = atoi(wd);
            p = scan_word(p, wd);
            y = atoi(wd);
            p = scan_word(p, wd);
            name = wd[0];
            p = scan_word(p, wd);
            for (char* q = wd; *q != '\0'; q++) {
                if (*q == '/') *q = '\n';
                if (*q == '.') *q = ' ';
            }
            strncpy(s, wd, BUFSIZ);
            strncat(s, "\n", BUFSIZ);
            p = scan_word(p, wd);
            if (p == NULL) {
                dirs = 0x0f;
            } else {
                dirs = 0;
                for (char* q = wd; *q != '\0'; q++) {
                    if (*q == 'U') dirs |= 0x01;
                    else if (*q == 'D') dirs |= 0x02;
                    else if (*q == 'L') dirs |= 0x04;
                    else if (*q == 'R') dirs |= 0x08;
                    else printf("Unknown direction %c\n", *q);
                }
            }
            add_tile(x, y, name, s, dirs);
        } else if (strcmp(wd, "GOAL") == 0) {
            int i, x, y;
            char name;
            TILE *t;
            p = scan_word(p, wd);
            i = atoi(wd);
            if (i <= 0 || MAX_GOALS < i) {
                printf("Bad goal number %d\n",i);
                return 1;
            }
            p = scan_word(p, wd);
            name = wd[0];
            p = scan_word(p, wd);
            x = atoi(wd);
            p = scan_word(p, wd);
            y = atoi(wd);
            t = board_find_tile(name);
            if (t == NULL) {
                printf("Unknown tile name: %c\n", name);
                return 1;
            }
            goal_size = i < goal_size ? goal_size : i;
            goal_tile[i-1] = t;
            goal_pos[i-1] = board_pos(x, y);
        } else {
            printf("Unknown input format: %s\n", buf);
            return 1;
        }
    }
    return 0;
}

int main(int argc, char* argv[]) {
    int hash_entry_size = 1299709;
    int hash_data_size = 16*1024*1024;
    int max_depth = 150;
    int inc_depth = 10;

    for (int i = 1; i < argc; i++) {
        if (strcmp(argv[i], "-big") == 0) {
            hash_entry_size = 32452843;
            hash_data_size = 256*1024*1024;
        } else if (strncmp(argv[i], "-d=", 3) == 0) {
            max_depth = atoi(argv[i] + 3);
        } else if (strncmp(argv[i], "-i=", 3) == 0) {
            inc_depth = atoi(argv[i] + 3);
        } else {
            error("Unknown option");
        }
    }
    if (read_puzzle()) {
        return 1;
    }
    board_init();
    hash_init(hash_entry_size, hash_data_size);
    board_print();
    printf("\n");
    solve(max_depth, inc_depth);
    return 0;
}
